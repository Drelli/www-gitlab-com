---
layout: markdown_page
title: Product Direction - Fulfillment - Self-Service Purchase
description: "The Fulfillment team at GitLab focuses on creating and supporting the enablement of our customers to purchase, upgrade, downgrade, and renew licenses and subscriptions."
canonical_path: "/direction/fulfillment/"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
 
Last reviewed: 2022-08

## Fulfillment: Self-Service Purchase Overview

Self-service Purchase, a [Fulfillment](https://about.gitlab.com/direction/fulfillment/) subgroup, is responsible for our primary e-commerce experience, with the goal of increasing the number of first-orders we receive through our self-service purchase flow and in turn, reducing the need for a GitLab team member to manually intervene.

## Strategic Fit, User Value, and Business Value

### Strategic Fit: What is the business context of self-service purchasing?

How the self-service purchase mission aligns to GitLab's broader strategy:

- **GitLab Mission & Vision**: [GitLab's mission](https://about.gitlab.com/company/mission/#mission) to make it so that everyone can contribute. [GitLab's vision](https://about.gitlab.com/company/vision/#vision) is to **develop great open source software** to enable people to collaborate in this way. GitLab is a single application based on convention over configuration that everyone should be able to afford and adapt.
- **Company Strategy**: [Our strategic goal](https://about.gitlab.com/company/strategy/) is to be known as the leading complete DevOps Platform. By using a DevOps platform to replace multiple point solutions, GitLab customers can achieve cost saving and efficiency gain. Therefore, when **GitLab develops more features to improve the product maturity**, it becomes easier to replace point solutions and GitLab will attract more users.
- **[Product Strategy](https://about.gitlab.com/direction/#product-strategy)**:
   - Focus on increasing Stages per Organization: There is a **strong correlation between the number of stages customers use and their propensity to upgrade to a paid package**. In fact, adding a stage triples conversion!
   - Harness the unique power of a single application
   - Increase wider-community contributions
   - Make our core journey categories lovable
   - GitLab-hosted first
- **Fulfillment Mission**: Our goal is to **make it easy for customers to purchase**, activate, and manage their GitLab subscriptions. By making it easier for customers to transact and manage their subscriptions, we increase customer satisfaction and improve our go-to-market (GTM) efficiency.
- **Self-Service Purhase Mission**: Improving GitLab's self-service purchasing allows customers to engage with us on their terms: interacting with us should be flexible, asynchronous enabled, and transparent.

### User Value: What is the hypothesis around the user problem and opportunity?

still need to validate the following via research:

User persona: [buyer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/)

- **Buyer**: Having an easy, reliable way to purchase from GitLab is a factor that influences my buying decision. 

### Business Value: What is the hypothesis around the business impact of self-service purchasing?

still need to validate the following via research:

- **Engineering**: If we have a vendor, we will have less custom code to maintain. We will get to take advantage of e-commerce best practices.
- **Executive**: If we have a vendor, we will be able to accelerate time to features that will help us increase first-orders.
- **Finance**: If we have a vendor, it will cost us less than a home grown solution over time.
- **Account Executive / Support**: If we have a vendor, I expect my customers not to have issues with common self-service functionality -- renewals, licensing, QSR, buying, and account management.

## 1-year Plan

- FY'23-Q3: Validate user and business problems. Make vendor no-go / go decision. Define MVP.
- FY'23-Q4: Assuming we pick a vendor, execute upon MVP.
- FY'24-Q1: Vendor integration, build & launch.
- FY'24-Q2: Vendor integration, build & launch.

## Performance Indicators

- Find our performance indicators [here](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/fulfillment-section/).
